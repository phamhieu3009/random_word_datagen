import numpy as np
import pickle as pkl
from tqdm import tqdm
import logging
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)


min_num = 1
max_num = 18
quota = 50000
pad_limit = 150

all_vocab = pkl.load(open("all_vocab.pkl", "rb")) 

all_vocab = list(all_vocab)


logger.info("Preparation complete, beginning generate random seqs")
random_token_nums = np.random.randint(min_num, max_num, quota)



with open("output/text_data_random.txt", "w") as f:
    with tqdm(total = len(random_token_nums)) as pbar:
        for num in random_token_nums: 
            token_indices  = np.random.choice(len(all_vocab), num)
            token_seq = " ".join([all_vocab[i] for i in token_indices])
            if len(token_seq) > pad_limit:
                token_seq = token_seq[:pad_limit]
            f.write(token_seq + "\n")
            pbar.update(1)

