import inspect
import os
from pathlib import Path
from text_renderer.effect.base_effect import NoEffects
from text_renderer.effect.base_effect import Effects
import imgaug.augmenters as iaa
from dataclasses import dataclass
from pathlib import Path
from typing import List, Tuple, Union
from PIL.Image import Image as PILImage
import numpy as np

from text_renderer.effect import *
from text_renderer.corpus import *
from text_renderer.config import (
    RenderCfg,
    NormPerspectiveTransformCfg,
    GeneratorCfg,
    FixedTextColorCfg,
    SimpleTextColorCfg,
)
from text_renderer.layout.same_line import SameLineLayout
from text_renderer.layout.extra_text_line import ExtraTextLineLayout
from glob import glob

from dataclasses import dataclass, field
from pathlib import Path
from typing import List

import numpy as np
from loguru import logger
from text_renderer.utils.errors import PanicError
from text_renderer.utils.utils import random_choice

from text_renderer.corpus.corpus import Corpus, CorpusCfg
from iter_corpus import IterCorpus
from text_renderer.effect.curve import Curve
import imgaug.augmenters as iaa

CURRENT_DIR = Path(os.path.abspath(os.path.dirname(__file__)))
OUT_DIR = CURRENT_DIR / "output_image"
DATA_DIR = CURRENT_DIR / "data"
BG_DIR = DATA_DIR / "bg"
CHAR_DIR = DATA_DIR / "char"
FONT_DIR = DATA_DIR / "fonts/vie"
FONT_LIST_DIR = DATA_DIR 
TEXT_DIR = DATA_DIR / "text"

font_cfg = dict(
    font_dir=FONT_DIR,
    font_list_file=FONT_LIST_DIR / "vie_common_cmnd.txt",
    font_size=(40, 41),
)


font_cfg_extra = dict(
    font_dir=FONT_DIR,
    font_list_file=FONT_LIST_DIR / "vie_common_cmnd.txt",
    font_size=(40, 41),
)

# calibrate text brightness and color
@dataclass
class CustomTextColorCfg(SimpleTextColorCfg):
    """
    Randomly use mean value of background image
    """

    alpha: Tuple[int, int] = (225, 255)

    def get_color(self, bg_img: PILImage) -> Tuple[int, int, int, int]:
        np_img = np.array(bg_img)
        mean = np.mean(np_img)

        alpha = np.random.randint(*self.alpha)
        r = np.random.randint(0, int(mean * 0.7))
        g = np.random.randint(0, int(mean * 0.7))
        b = np.random.randint(0, int(mean * 0.7))
        text_color = (r, g, b, alpha)

        return text_color

perspective_transform = NormPerspectiveTransformCfg(12, 12, 1)



def base_cfg(
    name: str, corpus, corpus_effects=None, layout_effects=None, layout=None, gray=True,
text_color_cfg = None):
    return GeneratorCfg(
        num_image=50000,
        save_dir=OUT_DIR / name,
        render_cfg=RenderCfg(
            bg_dir=BG_DIR,
            perspective_transform=perspective_transform,
            gray=gray,
            layout_effects=layout_effects,
            layout=layout,
            corpus=corpus,
            corpus_effects=corpus_effects,
            text_color_cfg= text_color_cfg,
            height = 48
        ),
    )




def enum_data():
    cfg =  base_cfg(
        inspect.currentframe().f_code.co_name,
        corpus= [IterCorpus(
            EnumCorpusCfg(
                text_paths=[TEXT_DIR / "text_data_random.txt"],
                filter_by_chars=True,
                chars_file=CHAR_DIR / "vie_general.txt",  
                char_spacing = (-0.1,0.1),      
                **font_cfg
            )),
            
            EnumCorpus(EnumCorpusCfg(
                items=["THIS IS AN EXTRA TEXT LINE!", "fasfqwwfgwegFQRRGQEferhqRFQegbhwegQFGWQRHBqfQGVBGvavw", 
                            "..................................................", "fqfqwfw235rqqfacxvbdbbavcacaccq"],
                text_color_cfg=FixedTextColorCfg(),
                **font_cfg_extra))
            ], 

    corpus_effects = [Effects([
                            Line(p=0.1, thickness=(2, 3)),
                            DropoutRand(p=0.1, dropout_p=(0.001, 0.01)),   
                            DropoutHorizontal(p=0.1, num_line=3, thickness=1),
                            DropoutVertical(p=0.1, num_line=10),
                            Padding(p=0.1, w_ratio=[0.001, 0.01], h_ratio=[0.001, 0.01], center=False),
                            Curve(p=0.1, period=90, amplitude=(4, 5)),
                            ImgAugEffect(p = 0.1, aug=iaa.Emboss(alpha=(0.9, 1.0), strength=(1, 1.5)))
                            ]),

                    NoEffects()    
                        ],


    layout =  ExtraTextLineLayout(1),
    layout_effects= None,

    gray = False,
    text_color_cfg= CustomTextColorCfg())
    # cfg.render_cfg.corpus_effects = Effects(
    #     Padding(p=1, w_ratio=[0.2, 0.21], h_ratio=[0.7, 0.71], center=True))
    return cfg






def same_line_layout_different_font_size():
    cfg = base_cfg(inspect.currentframe().f_code.co_name)
    cfg.render_cfg.layout = SameLineLayout(h_spacing=(0.9, 0.91))
    cfg.render_cfg.corpus = [
        EnumCorpus(
            EnumCorpusCfg(
                items=["Hello "],
                text_color_cfg=FixedTextColorCfg(),
                **font_cfg,
            ),
        ),
        EnumCorpus(
            EnumCorpusCfg(
                items=[" World!"],
                text_color_cfg=FixedTextColorCfg(),
                **small_font_cfg,
            ),
        ),
    ]
    return cfg





def emboss():
    import imgaug.augmenters as iaa

    cfg = enum_data()
    cfg.render_cfg.height = 48
    cfg.render_cfg.corpus_effects = Effects(
        [
            ImgAugEffect(aug=iaa.Emboss(alpha=(0.9, 1.0), strength=(1.5, 1.6))),
        ]
    )
    return cfg


configs = [
    # bg_and_text_mask()
    # emboss()
    # vertical_text()
    # extra_text_line_layout()
    # char_spacing_compact(),
    # char_spacing_large(),
    # *line(),
    # perspective_transform(),
    # color_image(),
    # dropout_rand(),
    # dropout_horizontal(),
    # dropout_vertical(),
    # padding(),
    # same_line_layout_different_font_size(),
]

# dropout_rand()




configs = [
    enum_data()]
    # vie_word_data()]
    # same_line_data()]


