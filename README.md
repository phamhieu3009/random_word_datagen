# random_word_datagen

## 1. Creating corpus
Steps to do:
- Load datasets and merge into one corpus
- Extract and index unique words (split by whitespaces)
- Generate a list of random integers to act as token numbers for each line (min word/sent: 1, max: 18 tokens, 50000 numbers)
- Generate random token indices for each line
- For each line: get words by indices
- Create random words:
    + Calculate the number of words to replace with gibberish (0.15*word_number)
    + For i in range(number to replace):
        * Pick a random textline
        * Pich a random word in that textline
        * Creating random gibberish from chars in vie_general.txt and replace the selected word with it
        * Pad textline by 150 chars
- Write final output to a text file

## To do 
- Analyze results to improve