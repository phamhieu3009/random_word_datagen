from utils import allowed_char
from nltk import FreqDist
import pickle as pkl
from tqdm import tqdm
import numpy as np

word_dist = FreqDist()
textlines = [line.strip() for line in open("output/text_data_random.txt", "r").readlines()]

with tqdm(total = len(textlines)) as pbar:
        for line in textlines:
            # print(line)
            word_dist.update(line.split())
            pbar.update(1)


num_word = len(word_dist.keys())
# print(num_word)
pct_random = 0.15
num_random_words = int(0.15*num_word)
pad_limit = 150
# pick a random sentence index
# pick a random word index
# replace the selected word with a gibberish

with tqdm(total = num_random_words) as pbar:
    for i in range(num_random_words):
        random_sent_index = np.random.choice(len(textlines))
        sentence = textlines[random_sent_index].split()
        random_word_index = np.random.choice(len(sentence))
        
        # creating gibberish
        gibberish_len = np.random.choice(list(range(3, 10)))
        gibberish = "".join(np.random.choice(allowed_char, gibberish_len))
        
        # replacing an original word with gibberish in a sentence
        sentence[random_word_index] = gibberish
        sentence = " ".join(sentence)
        if len(sentence) > pad_limit:
                sentence = sentence[:pad_limit]
        textlines[random_sent_index] =  sentence

        # print(gibberish)
        pbar.update(1)

# dump results to file
with open("output/text_data_random_final.txt", "w") as f:
    f.write("\n".join(textlines))