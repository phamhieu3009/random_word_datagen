import matplotlib.pyplot as plt
from collections import Counter




with open("output/fb_comment_10m_news_corpus_final.txt") as f:
    data = [line.strip() for line in f.readlines()]


sent_len = [len(line) for line in data]
plt.hist(sent_len)
plt.savefig("images/sent_length_by_char.png")

word_len_counter = Counter()
for line in data:
    word_lens = [len(word) for word in line.split()]
    word_len_counter.update(word_lens)
plt.bar(word_len_counter.keys(), word_len_counter.values())
plt.savefig("images/word_length_by_char.png")