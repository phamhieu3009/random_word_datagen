import json
from utils import clean_text
import pickle as pkl
import os
from tqdm import tqdm
from glob import glob


data_paths = json.load(open("data_config.json", "r"))
os.makedirs("./cache", exist_ok=True)


if not os.path.isfile(data_paths["fb_comment_10m"]["cache"]):

    textlines = []
    with tqdm(total = 10**7) as pbar:
        with open(data_paths["fb_comment_10m"]["orig"], "r") as f:
            for line in f:
                line = json.loads(line)["content"]
                # print(line)
                line = clean_text(line)
                textlines.append(line)
                pbar.update(1)
    print(len(textlines))
    with open(data_paths["fb_comment_10m"]["cache"], "wb") as f:
        pkl.dump(textlines, f)



    # textlines = []
paths = glob(f"""{data_paths["news_corpus"]["orig"]}/*.txt""")

with tqdm(total = len(paths)) as pbar:
        for path in paths:
            with open(path, "r") as f:
                filename = os.path.basename(path)
                data = [clean_text(line.strip()) for line in f.readlines()]
                # textlines += data
                with open(f"""cache/{filename.replace(".txt", ".pkl")}""", "wb") as f:
                    pkl.dump(data, f)
                pbar.update(1)

    # print(len(textlines))
    

