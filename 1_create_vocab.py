import os
import json
import pickle as pkl
from glob import glob
from tqdm import tqdm
data_paths = glob("./cache/*.pkl")
from multiprocessing import Pool
from nltk import FreqDist
from utils import clean_text

import logging
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)


def extract_unique_words(data_path):
    logger.info(f"Processing {data_path}")
    word_dist = FreqDist()
    textlines = pkl.load(open(data_path, "rb"))
    with tqdm(total = len(textlines)) as pbar:
        for line in textlines:
            word_dist.update(line.split())
            
            pbar.update(1)

    logger.info(f"Finished processing {data_path}")
    logger.info("Extracting unique word")
    return list(word_dist.keys())


if __name__ == '__main__':
    with Pool(len(data_paths)) as p:
        data = p.map(extract_unique_words, data_paths)
    
    all_vocab = set()
    for datum in data:
        all_vocab = all_vocab.union(datum)

    
    with open("all_vocab.pkl", "wb") as f:
                    pkl.dump(all_vocab, f)

    print(all_vocab)

